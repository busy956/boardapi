package com.bj.boardapi.controller;

import com.bj.boardapi.entity.Board;
import com.bj.boardapi.model.BoardItem;
import com.bj.boardapi.model.BoardRequest;
import com.bj.boardapi.model.BoardResponse;
import com.bj.boardapi.model.BoardTitleChangeRequest;
import com.bj.boardapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1//board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/people")
    public String setBoard(@RequestBody BoardRequest boardRequest) {
        boardService.setBoard(boardRequest);

        return "OK";
    }

    @GetMapping("/all")
    public List<BoardItem> getBoards() {
        return boardService.getBoards();
    }

    @GetMapping("/detail/{id}")
    public BoardResponse getBoard(@PathVariable long id) {
        return boardService.getBoard(id);
    }

    @PutMapping("/title/{id}")
    public String putBoard(@PathVariable long id, @RequestBody BoardTitleChangeRequest request) {
        boardService.putBoard(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return "삭제완료";
    }
}
