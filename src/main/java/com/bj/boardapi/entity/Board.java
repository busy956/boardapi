package com.bj.boardapi.entity;

import com.bj.boardapi.enums.BoardType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 40)
    private String title;

    @Column(nullable = false, length = 20)
    private String writer;

    @Column(nullable = false)
    private LocalDate boardDate;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private BoardType boardType;

    @Column(nullable = false)
    private Integer viewNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
