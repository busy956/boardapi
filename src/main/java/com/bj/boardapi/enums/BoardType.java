package com.bj.boardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BoardType {
    ilban("일반"),
    jayu("자유"),
    jilmun("질문");

    private final String typeName;
}
