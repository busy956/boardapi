package com.bj.boardapi.service;

import com.bj.boardapi.entity.Board;
import com.bj.boardapi.model.BoardItem;
import com.bj.boardapi.model.BoardRequest;
import com.bj.boardapi.model.BoardResponse;
import com.bj.boardapi.model.BoardTitleChangeRequest;
import com.bj.boardapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(BoardRequest request) {
        Board addData = new Board();
        addData.setTitle(request.getTitle());
        addData.setWriter(request.getWriter());
        addData.setBoardDate(LocalDate.now());
        addData.setBoardType(request.getBoardType());
        addData.setViewNumber(request.getViewNumber());
        addData.setEtcMemo(request.getEtcMemo());

        boardRepository.save(addData);
    }

    public List<BoardItem> getBoards() {
        List<Board> originData = boardRepository.findAll();

        List<BoardItem> addData = new LinkedList<>();

        for (Board board : originData) {
            BoardItem request = new BoardItem();
            request.setId(board.getId());
            request.setTitle(board.getTitle());
            request.setWriter(board.getWriter());
            request.setBoardDate(board.getBoardDate());
            request.setViewNumber(board.getViewNumber());

            addData.add(request);
        }
        return addData;
    }

    public BoardResponse getBoard(Long id) {
        Board originData = boardRepository.findById(id).orElseThrow();
        BoardResponse response = new BoardResponse();
        response.setId(originData.getId());
        response.setTitle(originData.getTitle());
        response.setWriter(originData.getWriter());
        response.setBoardDate(originData.getBoardDate());
        response.setBoardType(originData.getBoardType());
        response.setViewNumber(originData.getViewNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putBoard(long id, BoardTitleChangeRequest request) {
        Board originData = boardRepository.findById(id).orElseThrow();
        originData.setTitle(request.getTitle());

        boardRepository.save(originData);
    }

    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
