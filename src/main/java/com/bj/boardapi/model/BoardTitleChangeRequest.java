package com.bj.boardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardTitleChangeRequest {
    private String title;
}
