package com.bj.boardapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardItem {
    private Long id;
    private String title;
    private String writer;
    private LocalDate boardDate;
    private Integer viewNumber;
}
