package com.bj.boardapi.model;

import com.bj.boardapi.enums.BoardType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardRequest {
    private String title;
    private String writer;
    private LocalDate boardDate;
    private BoardType boardType;
    private Integer viewNumber;
    private String etcMemo;
}
